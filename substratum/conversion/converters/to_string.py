# encoding: UTF-8

import six
from substratum.conversion.support import AbstractTypeConverter


class ToStringConverter(AbstractTypeConverter):
    def _can_convert(self, source_type, target_type):
        return issubclass(target_type, six.string_types)

    def _do_convert(self, source, target_type, member_info):
        return target_type(source)
