# encoding: UTF-8

import six

from substratum.conversion.support import AbstractTypeConverter
from substratum.core.annotation import Annotation, ElementType


class IntegerFormat(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(format):
        pass


class IntegerTypeConverter(AbstractTypeConverter):
    __BASE_MAPPING = {
        "b": 2,
        "o": 8,
        "d": 10,
        "x": 16
    }

    def __init__(self):
        self.__format = "d"

    @property
    def format(self):
        return self.__format

    @format.setter
    def format(self, value):
        self.__format = value

    def _can_convert(self, source_type, target_type):
        return (
            (issubclass(source_type, six.string_types) and issubclass(target_type, six.integer_types)) or
            (issubclass(source_type, six.integer_types) and issubclass(target_type, six.string_types))
        )

    def _do_convert(self, source, target_type, member_info):
        if member_info:
            a_format = member_info.get_annotation(IntegerFormat)

            format = getattr(a_format, "format", None)
        else:
            format = None

        if issubclass(target_type, six.integer_types):
            return target_type(source, self.__BASE_MAPPING[format or self.__format])
        else:
            return target_type("{0:%s}" % (format or self.__format)).format(source)
