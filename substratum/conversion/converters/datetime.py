# encoding: UTF-8

from __future__ import absolute_import

from datetime import datetime, date, time

import six

from substratum.conversion.support import AbstractTypeConverter
from substratum.core.annotation import Annotation, ElementType


class DateTimeFormat(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(format):
        pass


class DateTimeConverter(AbstractTypeConverter):
    def __init__(self):
        self.__datetime_format = "%Y-%m-%dT%H:%M:%S.%f"
        self.__date_format = "%Y-%m-%d"
        self.__time_format = "%H:%M:%S.%f"

    @property
    def datetime_format(self):
        return self.__datetime_format

    @datetime_format.setter
    def datetime_format(self, value):
        self.__datetime_format = value

    def _can_convert(self, source_type, target_type):
        return (
            (issubclass(source_type, six.string_types) and issubclass(target_type, (datetime, date, time))) or
            (issubclass(source_type, (datetime, date, time)) and issubclass(target_type, six.string_types))
        )

    def _do_convert(self, source, target_type, member_info):
        if member_info:
            a_format = member_info.get_annotation(DateTimeFormat)

            format = getattr(a_format, "format", None)
        else:
            format = None

        if issubclass(target_type, datetime):
            return datetime.strptime(source, format or self.__datetime_format)
        elif issubclass(target_type, date):
            return datetime.strptime(source, format or self.__date_format).date()
        elif issubclass(target_type, time):
            return datetime.strptime(source, format or self.__time_format).time()
        elif isinstance(source, datetime):
            return source.strftime(format or self.__datetime_format)
        elif isinstance(source, date):
            return source.strftime(format or self.__date_format)
        elif isinstance(source, time):
            return source.strftime(format or self.__time_format)
