# encoding: UTF-8

from abc import *

from substratum.conversion.exception import ConversionException
from substratum.conversion.interfaces import ITypeConverter


class AbstractTypeConverter(ITypeConverter):
    __metaclass__ = ABCMeta

    @abstractmethod
    def _can_convert(self, source_type, target_type):
        """

        :type source_type: type
        :type target_type: type
        :rtype: bool
        """

    @abstractmethod
    def _do_convert(self, source, target_type, member_info):
        """

        :type source: object
        :type target_type: type
        :type member_info: substratum.core.reflection.info.AbstractMemberInfo
        :rtype: object
        """

    def convert_if_necessary(self, source, target_type, member_info=None):
        source_type = type(source)

        if source_type == target_type:
            return source

        if not self._can_convert(source_type, target_type):
            raise ConversionException("Can not convert %r to %s" % (source, target_type))

        return self._do_convert(source, target_type, member_info)


class ConversionService(ITypeConverter):
    def __init__(self):
        self.__converters = []

    @property
    def converters(self):
        return self.__converters

    @converters.setter
    def converters(self, converters):
        self.__converters = converters

    def convert_if_necessary(self, source, target_type, member_info=None):
        if type(source) == target_type:
            return source

        for converter in self.__converters:
            try:
                return converter.convert_if_necessary(source, target_type, member_info)
            except ConversionException:
                continue

        raise ConversionException("Can not convert %r to %s" % (source, target_type))
