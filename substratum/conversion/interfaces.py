# encoding: UTF-8

from abc import *


class ITypeConverter(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def convert_if_necessary(self, source, target_type, member_info=None):
        """
        Convert source to target type.

        :type source: object
        :type target_type: type
        :type member_info: substratum.core.reflection.info.AbstractMemberInfo
        :rtype: object
        """
