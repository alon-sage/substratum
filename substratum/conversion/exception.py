# encoding: UTF-8

from substratum.core.exception import CoreException


class ConversionException(CoreException):
    pass
