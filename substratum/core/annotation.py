# encoding: UTF-8

import inspect

import six


class _AnnotationHolder(object):
    __instance = None
    __annotations = {}

    def __new__(cls):
        if cls.__instance is None:
            cls.__instance = super(_AnnotationHolder, cls).__new__(cls)
        return cls.__instance

    def set_annotation(self, obj, annotation):
        if not isinstance(annotation, Annotation):
            raise TypeError("%r is not annotation" % annotation)

        if not inspect.isclass(obj) and not inspect.isfunction(obj) and not isinstance(obj, property):
            raise TypeError("Can not annotate %r" % obj)

        if inspect.isclass(obj) and ElementType.CLASS not in annotation.__target__:
            raise TypeError("%s not applicable to classes" % annotation)
        elif inspect.isfunction(obj) and ElementType.METHOD not in annotation.__target__:
            raise TypeError("%s not applicable to methods" % annotation)
        elif isinstance(obj, property) and ElementType.PROPERTY not in annotation.__target__:
            raise TypeError("%s not applicable to properties" % annotation)

        if isinstance(obj, property):
            obj = obj.fget or obj

        if obj not in self.__annotations:
            self.__annotations[obj] = {}
        annotations = self.__annotations[obj]

        annotation_type = type(annotation)

        if annotation.__repeatable__:
            if annotation_type not in annotations:
                annotations[annotation_type] = []
            annotations[annotation_type].append(annotation)
        else:
            if annotation_type not in annotations:
                annotations[annotation_type] = annotation
            else:
                raise TypeError("%s is not repeatable" % annotation)

    def __check_annotation_type(self, annotation_type):
        if not issubclass(annotation_type, Annotation):
            raise TypeError("%r is not annotation class" % annotation_type)

    def __check_object(self, obj):
        if not inspect.isclass(obj) and not inspect.ismethod(obj) and not isinstance(obj, property):
            raise TypeError("Can not get annotation from %r" % obj)

    def has_annotation(self, obj, annotation_type):
        self.__check_annotation_type(annotation_type)
        self.__check_object(obj)

        if inspect.ismethod(obj):
            obj = six.get_method_function(obj)
        elif isinstance(obj, property):
            obj = obj.fget or obj

        if obj not in self.__annotations:
            return False

        return annotation_type in self.__annotations[obj]

    def get_annotation(self, obj, annotation_type):
        self.__check_annotation_type(annotation_type)
        self.__check_object(obj)

        if inspect.ismethod(obj):
            obj = six.get_method_function(obj)
        elif isinstance(obj, property):
            obj = obj.fget or obj

        if obj not in self.__annotations:
            return None

        return self.__annotations[obj].get(annotation_type, None)

    def get_annotations(self, obj):
        self.__check_object(obj)

        if inspect.ismethod(obj):
            obj = six.get_method_function(obj)
        elif isinstance(obj, property):
            obj = obj.fget or obj

        annotations = self.__annotations.get(obj, None)
        if annotations:
            return sum(map(lambda x: x if isinstance(x, list) else [x], annotations.values()), [])
        else:
            return []


class _AnnotationBase(object):
    __target__ = []
    __repeatable__ = False
    __attributes__ = []

    @staticmethod
    def _validate_attributes():
        pass

    __holder = _AnnotationHolder()

    def __new__(cls, *args, **attributes):
        args, _, _, _ = inspect.getargspec(cls._validate_attributes)
        for name in args:
            attributes.setdefault(name, None)

        try:
            cls._validate_attributes(**attributes)
        except TypeError as e:
            raise TypeError(
                e.message.replace("_validate_attributes()", "Annotation<%s.%s>" % (cls.__module__, cls.__name__)))

        instance = super(_AnnotationBase, cls).__new__(cls)
        instance.__attributes__ = attributes.keys()
        for k, v in attributes.items():
            setattr(instance, k, v)
        return instance

    def __call__(self, obj):
        self.__holder.set_annotation(obj, self)
        return obj

    def __repr__(self):
        return "Annotation<%s.%s>" % (self.__class__.__module__, self.__class__.__name__)


class _AnnotationMeta(type):
    def __call__(self, _=None, **attributes):
        annotation = super(_AnnotationMeta, self).__call__(None, **attributes)

        if _ is None:
            return annotation
        else:
            return annotation(_)


class ElementType(object):
    CLASS = 1
    PROPERTY = 2
    METHOD = 3


class Annotation(_AnnotationBase):
    __metaclass__ = _AnnotationMeta


class AnnotationUtils(object):
    __holder = _AnnotationHolder()

    @classmethod
    def has_annotation(cls, obj, annotation_type):
        return cls.__holder.has_annotation(obj, annotation_type)

    @classmethod
    def get_annotation(cls, obj, annotation_type):
        return cls.__holder.get_annotation(obj, annotation_type)

    @classmethod
    def get_annotations(cls, obj):
        return cls.__holder.get_annotations(obj)
