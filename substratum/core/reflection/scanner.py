# encoding: UTF-8

import inspect
import pkgutil
import importlib


class ModuleScanner(object):
    def __init__(self, modules):
        self.__modules = modules

    def __iter_module_types(self, module):
        for name, type in inspect.getmembers(module, inspect.isclass):
            if type.__module__ == module.__name__:
                yield name, type

    def __iter__(self):
        for module in self.__modules:
            if hasattr(module, "__path__"):
                for loader, name, is_pkg in pkgutil.walk_packages(module.__path__, module.__name__ + "."):
                    inner_module = importlib.import_module(name, module.__name__)
                    for type_name, type in self.__iter_module_types(inner_module):
                        yield type_name, type
            else:
                for type_name, type in self.__iter_module_types(module):
                    yield type_name, type
