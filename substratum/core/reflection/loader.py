# encoding: UTF-8

import importlib


class ClassLoader(object):
    def __init__(self, package=None):
        self.__package = package

    def load_class(self, name):
        if "." in name:
            module_name, class_name = name.rsplit(".", 1)

            if self.__package and not module_name.startswith("."):
                module_name = "." + module_name

            module = importlib.import_module(module_name, self.__package)
        elif self.__package:
            class_name = name
            module = importlib.import_module(self.__package)
        else:
            raise ImportError("No module specified")

        if hasattr(module, class_name):
            return getattr(module, class_name)
        else:
            raise ImportError("Module %s has no class %s" % (module.__name__, class_name))
