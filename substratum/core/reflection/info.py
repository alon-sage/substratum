# encoding: UTF-8

import inspect
from abc import *

from substratum.core.annotation import AnnotationUtils


class AbstractInfo(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, name, described):
        self.__name = name
        self.__described = described

    @property
    def name(self):
        return self.__name

    @property
    def _described(self):
        return self.__described

    def has_annotation(self, annotation_type):
        return AnnotationUtils.has_annotation(self.__described, annotation_type)

    def get_annotation(self, annotation_type):
        return AnnotationUtils.get_annotation(self.__described, annotation_type)

    def get_annotations(self):
        return AnnotationUtils.get_annotations(self.__described)


class AbstractMemberInfo(AbstractInfo):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, class_info, name, member):
        super(AbstractMemberInfo, self).__init__(name, member)
        self.__class_info = class_info
        self.__is_public = not name.startswith("_")
        self.__is_magic = name.startswith("__") and name.endswith("__")

    @property
    def class_info(self):
        """:rtype: ClassInfo"""
        return self.__class_info

    @property
    def is_public(self):
        return self.__is_public

    @property
    def is_magic(self):
        return self.__is_magic

    def get(self, obj):
        return getattr(obj, self.name)

    def set(self, obj, value):
        setattr(obj, self.name, value)

    def delete(self, obj):
        delattr(obj, self.name)


class AbstractMethodInfo(AbstractMemberInfo):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, class_info, name, method):
        super(AbstractMethodInfo, self).__init__(class_info, name, method)
        if inspect.ismethod(method):
            self.__args, varargs, keywords, defaults = inspect.getargspec(method)
            if self.__args:
                self.__args.pop(0)
            self.__has_varargs = bool(varargs)
            self.__has_keywords = bool(keywords)
            self.__defaults = {}

            if defaults is None:
                defaults = []
            else:
                defaults = list(defaults)

            if len(defaults) > len(self.__args):
                defaults.pop(0)

            offset = len(self.__args) - len(defaults)
            arg_reprs = []

            for i, arg in enumerate(self.__args):
                if i >= offset:
                    self.__defaults[arg] = defaults[i - offset]
                    arg_reprs.append("%s=%r" % (arg, defaults[i - offset]))
                else:
                    arg_reprs.append(arg)

            if varargs:
                arg_reprs.append("*%s" % varargs)

            if keywords:
                arg_reprs.append("**%s" % keywords)
        else:
            self.__args = []
            self.__has_varargs = True
            self.__has_keywords = True
            self.__defaults = []
            arg_reprs = ["*args", "**kwargs"]

        self.__repr = "%s<%s.%s.%s(%s)>" % (
            self.__class__.__name__.lstrip("_"), class_info.module, class_info.name, name, ", ".join(arg_reprs)
        )

    @property
    def args(self):
        return self.__args

    @property
    def has_varargs(self):
        return self.__has_varargs

    @property
    def has_keywords(self):
        return self.__has_keywords

    @property
    def defaults(self):
        return self.__defaults

    def __repr__(self):
        return self.__repr


class ConstructorInfo(AbstractMethodInfo):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, class_info, name, init_method, ctor):
        super(ConstructorInfo, self).__init__(class_info, name, init_method)
        self.__ctor = ctor

    def new_instance(self, *args, **kwargs):
        return self.__ctor(*args, **kwargs)

    @staticmethod
    def from_class(cls):
        return ClassInfo(cls).constructor

    @staticmethod
    def from_object(obj):
        return ClassInfo(type(obj)).constructor

    def has_annotation(self, annotation_type):
        try:
            return super(ConstructorInfo, self).has_annotation(annotation_type)
        except TypeError:
            return False

    def get_annotation(self, annotation_type):
        try:
            return super(ConstructorInfo, self).get_annotation(annotation_type)
        except TypeError:
            return None

    def get_annotations(self):
        try:
            return super(ConstructorInfo, self).get_annotations()
        except TypeError:
            return []


class MethodInfo(AbstractMethodInfo):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, class_info, name, method):
        super(MethodInfo, self).__init__(class_info, name, method)

    def invoke_method(self, obj, *args, **kwargs):
        return getattr(obj, self.name)(*args, **kwargs)

    @staticmethod
    def from_class(cls, name):
        return ClassInfo(cls).get_method(name)

    @staticmethod
    def from_object(obj, name):
        return ClassInfo(type(obj)).get_method(name)


class PropertyInfo(AbstractMemberInfo):
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, class_info, name, property):
        super(PropertyInfo, self).__init__(class_info, name, property)
        self.__repr = "%s<%s.%s.%s>" % (
            self.__class__.__name__.lstrip('_'), class_info.module, class_info.name, name
        )

    @property
    def readable(self):
        return self._described.fget is not None

    @property
    def writeable(self):
        return self._described.fset is not None

    @property
    def deletable(self):
        return self._described.fdel is not None

    def get(self, obj):
        return self._described.__get__(obj, type(obj))

    def set(self, obj, value):
        self._described.__set__(obj, value)

    def delete(self, obj):
        self._described.__delete__(obj)

    def __repr__(self):
        return self.__repr

    @staticmethod
    def from_class(cls, name):
        return ClassInfo(cls).get_property(name)

    @staticmethod
    def from_object(obj, name):
        return ClassInfo(type(obj)).get_property(name)


class ClassInfo(AbstractInfo):
    __cache = {}

    class _ConstructorInfo(ConstructorInfo):
        def __init__(self, class_info, name, init_method, ctor):
            super(ClassInfo._ConstructorInfo, self).__init__(class_info, name, init_method, ctor)

    class _MethodInfo(MethodInfo):
        def __init__(self, class_info, name, method):
            super(ClassInfo._MethodInfo, self).__init__(class_info, name, method)

    class _PropertyInfo(PropertyInfo):
        def __init__(self, class_info, name, property):
            super(ClassInfo._PropertyInfo, self).__init__(class_info, name, property)

    def __new__(cls, _):
        if not inspect.isclass(_):
            raise TypeError("first argument must be a class")

        if _ not in cls.__cache:
            cls.__cache[_] = super(ClassInfo, cls).__new__(cls, _)
        return cls.__cache[_]

    __initialized = False

    def __init__(self, cls):
        if self.__initialized:
            return

        self.__initialized = True

        super(ClassInfo, self).__init__(cls.__name__, cls)

        self.__module = cls.__module__

        self.__cls = cls
        self.__bases = cls.__bases__
        self.__mro = inspect.getmro(cls)

        self.__constructor = self._ConstructorInfo(self, "!ctor", getattr(cls, "__init__"), cls)

        self.__properties = {}
        self.__methods = {}
        for name, member in inspect.getmembers(cls):
            if isinstance(member, property):
                self.__properties[name] = self._PropertyInfo(self, name, member)
            if inspect.ismethod(member):
                if name != "__init__":
                    self.__methods[name] = self._MethodInfo(self, name, member)

        self.__repr = "%s<%s.%s>" % (self.__class__.__name__, self.module, self.name)

    @property
    def module(self):
        return self.__module

    @property
    def cls(self):
        return self.__cls

    @property
    def canonical_name(self):
        return self.module + "." + self.name

    @property
    def bases(self):
        return self.__bases

    @property
    def mro(self):
        return self.__mro

    @property
    def constructor(self):
        return self.__constructor

    def has_property(self, name):
        return name in self.__properties

    def get_property(self, name):
        """:rtype: PropertyInfo"""
        if name in self.__properties:
            return self.__properties[name]
        else:
            raise LookupError("Property '%s' at %s not found" % (name, self.__cls))

    def get_properties(self):
        """:rtype: list of PropertyInfo"""
        return self.__properties.values()

    def has_method(self, name):
        return name in self.__methods

    def get_method(self, name):
        """:rtype: MethodInfo"""
        if name in self.__methods:
            return self.__methods[name]
        else:
            raise LookupError("Method '%s' at %s not found" % (name, self.__cls))

    def get_methods(self):
        """:rtype: list of MethodInfo"""
        return self.__methods.values()

    def __repr__(self):
        return self.__repr

    @staticmethod
    def from_class(cls):
        return ClassInfo(cls)

    @staticmethod
    def from_object(obj):
        return ClassInfo(type(obj))
