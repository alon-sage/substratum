# encoding: UTF-8

import io


class _RawSocketIO(io.RawIOBase):
    def __init__(self, socket, closefd=True):
        super(_RawSocketIO, self).__init__()
        self.__socket = socket
        self.__closefd = closefd

    def fileno(self):
        return self.__socket.fileno()

    def close(self):
        if self.__closefd:
            self.__socket.close()

    def seekable(self):
        return False

    def readable(self):
        return True

    def readinto(self, b):
        return self.__socket.recv_into(b)

    def writable(self):
        return True

    def write(self, b):
        return self.__socket.send(b)


class SocketIO(io.BufferedRWPair):
    DEFAULT_BUFFER_SIZE = io.DEFAULT_BUFFER_SIZE

    def __init__(self, socket, buffer_size=DEFAULT_BUFFER_SIZE, closefd=True):
        self.__socket = socket
        raw = _RawSocketIO(socket, closefd)
        super(SocketIO, self).__init__(raw, raw, buffer_size)

    @property
    def socket(self):
        return self.__socket
