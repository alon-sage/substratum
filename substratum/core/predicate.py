# encoding: UTF-8


class Predicate(object):
    def __init__(self, func):
        self.__func = func

    def __call__(self, *args, **kwargs):
        return self.__func(*args, **kwargs)

    def __and__(self, other):
        return Predicate(lambda *args, **kwargs: self.__func(*args, **kwargs) and other.__func(*args, **kwargs))

    def __or__(self, other):
        return Predicate(lambda *args, **kwargs: self.__func(*args, **kwargs) or other.__func(*args, **kwargs))

    def __invert__(self):
        return Predicate(lambda *args, **kwargs: not self.__func(*args, **kwargs))


P = Predicate
