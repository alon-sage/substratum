# encoding: UTF-8

from contextlib import contextmanager

import sys
import traceback


class CoreException(Exception):
    def __init__(self, message=None, cause=None, *args):
        if message:
            super(CoreException, self).__init__(message, *args)
        else:
            super(CoreException, self).__init__(*args)
        self.__cause = cause

    @property
    def cause(self):
        return self.__cause

    @classmethod
    @contextmanager
    def wraps(cls, exc_types, message=None, *args):
        try:
            yield
        except exc_types:
            exc_type, exc_val, exc_tb = sys.exc_info()
            raise cls(message, (exc_type, exc_val, exc_tb.tb_next), *args)


@contextmanager
def suppress_exceptions(*exceptions):
    try:
        yield
    except tuple(exceptions):
        pass


def _print_exc(prefix, exc_info, f):
    if isinstance(exc_info, BaseException):
        exc_type = type(exc_info)
        exc_value = exc_info
        exc_tb = None
    else:
        exc_type, exc_value, exc_tb = exc_info

    if exc_value.message:
        print >> f, "%s %s \"%s\"" % (prefix, exc_type.__name__, exc_value.message),
    else:
        print >> f, "%s %s" % (prefix, exc_type.__name__),

    if exc_tb:
        print >> f, "at"
        print >> f, "".join((traceback.format_tb(exc_tb))),
    else:
        print >> f

    if issubclass(exc_type, CoreException) and exc_value.cause:
        _print_exc("Caused by", exc_value.cause, f)


def print_exception(exc_info=None, f=sys.stderr):
    if exc_info is None:
        exc_info = sys.exc_info()
    _print_exc("Caught", exc_info, f)
