# encoding: UTF-8

import logging


class LoggerAdapter(object):
    def __init__(self, logger, extra=None):
        self.__logger = logger
        self.__extra = extra or {}

    @property
    def extra(self):
        return self.__extra

    def _process(self, msg, args, kwargs):
        if "extra" in kwargs:
            kwargs["extra"].update(self.__extra)
        else:
            kwargs["extra"] = self.__extra

        return msg, args, kwargs

    def debug(self, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        self.__logger.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        self.__logger.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        self.__logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        self.__logger.error(msg, *args, **kwargs)

    def exception(self, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        kwargs["exc_info"] = 1
        self.__logger.exception(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        self.__logger.debug(msg, *args, **kwargs)

    def log(self, level, msg, *args, **kwargs):
        msg, args, kwargs = self._process(msg, args, kwargs)
        self.__logger.log(level, msg, *args, **kwargs)

    def isEnabledFor(self, level):
        return self.__logger.isEnabledFor(level)


class Logger(LoggerAdapter):
    def __init__(self, name, extra=None):
        super(Logger, self).__init__(logging.getLogger(name), extra)

    @classmethod
    def for_class(cls, type, *args, **kwargs):
        name = type.__module__ + "." + type.__name__
        return cls(name, *args, **kwargs)
