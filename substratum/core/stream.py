# encoding: UTF-8

import itertools


class Stream(object):
    def __init__(self, seq):
        self.__seq = seq
        self.__cls = self.__class__

    # Constructors

    @classmethod
    def count(cls, start=0, step=1):
        return cls(itertools.count(start, step))

    @classmethod
    def repeat(cls, obj, times=None):
        return cls(itertools.repeat(obj, times))

    @classmethod
    def cycle(cls, seq):
        return cls(itertools.cycle(seq))

    # Multi stream transformation

    @classmethod
    def zip(cls, seq1, seq2=None, *some):
        return cls(itertools.izip(seq1, seq2, *some))

    @classmethod
    def zip_longest(cls, seq1, seq2=None, *some, **kwargs):
        return cls(itertools.izip_longest(seq1, seq2, *some, **kwargs))

    @classmethod
    def chain(cls, *seqs):
        return cls(itertools.chain(*seqs))

    @classmethod
    def product(cls, *seqs):
        return cls(itertools.product(*seqs))

    @classmethod
    def apply(cls, func, *seqs):
        return cls(itertools.imap(func, *seqs))

    # Stream transformation

    def split(self, n=2):
        return [self.__cls(seq) for seq in itertools.tee(self.__seq, n)]

    def __getitem__(self, item):
        return self.__cls(itertools.islice(self.__seq, item.start, item.stop, item.step))

    def dropwhile(self, predicate):
        return self.__cls(itertools.dropwhile(predicate, self.__seq))

    def takewhile(self, predicate):
        return self.__cls(itertools.takewhile(predicate, self.__seq))

    def compress(self, selectors):
        return self.__cls(itertools.compress(self.__seq, selectors))

    def groupby(self, key=None):
        return self.__cls(itertools.groupby(self.__seq, key)).map(lambda x: (x[0], self.__cls(x[1])))

    def combinations(self, r):
        return self.__cls(itertools.combinations(self.__seq, r))

    def combinations_with_replacement(self, r):
        return self.__cls(itertools.combinations_with_replacement(self.__seq, r))

    def permutations(self, r):
        return self.__cls(itertools.permutations(self.__seq, r))

    def sort(self, key=None, reverse=False):
        """May be slow"""
        return self.__cls(sorted(self.__seq, key=key, reverse=reverse))

    # Element transformation

    def filter(self, predicate):
        return self.__cls(itertools.ifilter(predicate, self.__seq))

    def filterfalse(self, predicate):
        return self.__cls(itertools.ifilterfalse(predicate, self.__seq))

    def enumerate(self, start=0):
        return self.__cls(enumerate(self.__seq, start))

    def map(self, func):
        return self.__cls(itertools.imap(func, self.__seq))

    def starmap(self, func):
        return self.__cls(itertools.starmap(func, self.__seq))

    # Aggregation

    def as_tuple(self):
        return tuple(self.__seq)

    def as_list(self):
        return list(self.__seq)

    def as_set(self):
        return set(self.__seq)

    def as_dict(self):
        return dict(i for i in self.__seq)

    def reduce(self, func, initial=None):
        return reduce(func, self.__seq, initial)

    def __len__(self):
        length = 0
        for _ in self:
            length += 1
        return length

    # Element iteration

    def each(self, func, *args, **kwargs):
        for i in self.__seq:
            func(i, *args, **kwargs)

    def __iter__(self):
        for i in self.__seq:
            yield i
