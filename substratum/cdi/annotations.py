# encoding: UTF-8

from substratum.core.annotation import Annotation, ElementType


class Autowire(Annotation):
    """Autowire value from context to property or method arguments"""

    __target__ = [ElementType.METHOD, ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(**attributes):
        pass


class OnInit(Annotation):
    """Custom init-method definition."""

    __target__ = [ElementType.METHOD]


class OnDestroy(Annotation):
    """Custom destroy-method definition."""

    __target__ = [ElementType.METHOD]
