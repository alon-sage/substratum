# encoding: UTF-8

from abc import *
import weakref
import six

from substratum.cdi.annotations import OnInit, OnDestroy, Autowire
from substratum.cdi.interfaces import IObjectFactory, IApplicationContext, IApplicationListener, ILifeCycle
from substratum.cdi.interfaces import IInitializingObject
from substratum.cdi.interfaces import IDisposableObject
from substratum.cdi.interfaces import IObjectNameAware
from substratum.cdi.interfaces import IObjectFactoryAware
from substratum.cdi.interfaces import IApplicationEventPublisherAware
from substratum.cdi.interfaces import IApplicationContextAware
from substratum.core.reflection.info import ClassInfo


class AliasRegistry(IObjectFactory):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.__aliases = {}
        self.__object_aliases = {}

    def _register_object_name(self, canonical_name):
        if canonical_name in self.__object_aliases or canonical_name in self.__aliases:
            raise NameError("Object name '%s' is already in use" % canonical_name)

        self.__object_aliases[canonical_name] = []

    def _unregister_object_name(self, canonical_name):
        if canonical_name not in self.__object_aliases:
            raise LookupError("No object qualified by name '%s' found" % canonical_name)

        for alias in self.__object_aliases[canonical_name]:
            del self.__aliases[alias]
        del self.__object_aliases[canonical_name]

    def contains_object(self, object_name):
        return object_name in self.__object_aliases or object_name in self.__aliases

    def get_canonical_name(self, object_name):
        if object_name in self.__object_aliases:
            return object_name
        elif object_name in self.__aliases:
            return self.__aliases[object_name]
        else:
            raise LookupError("No object qualified by name '%s' found" % object_name)

    def get_aliases(self, object_name):
        canonical_name = self.get_canonical_name(object_name)

        return [canonical_name] + self.__object_aliases[canonical_name]

    def register_alias(self, object_name, new_alias):
        if new_alias in self.__aliases or new_alias in self.__object_aliases:
            raise NameError("Object name '%s' is already in use" % new_alias)

        canonical_name = self.get_canonical_name(object_name)

        self.__object_aliases[canonical_name].append(new_alias)
        self.__aliases[new_alias] = canonical_name

    def unregister_alias(self, existing_alias):
        canonical_name = self.get_canonical_name(existing_alias)

        if existing_alias == canonical_name:
            raise NameError("No alias '%s' found" % existing_alias)

        del self.__aliases[existing_alias]
        self.__object_aliases[canonical_name].remove(existing_alias)

    def get_objects_count(self):
        return len(self.__object_aliases)

    def get_objects_names(self):
        return self.__object_aliases.keys()


class ObjectDefinition(object):
    def __init__(self, object_name, object_type, is_singleton, depends_on=None):
        self.__object_name = object_name
        self.__object_type = object_type
        self.__is_singleton = is_singleton
        self.__class_info = ClassInfo(object_type)
        self.__is_object_name_aware = issubclass(object_type, IObjectNameAware)
        self.__is_object_factory_aware = issubclass(object_type, IObjectFactoryAware)
        self.__is_application_event_publisher_aware = issubclass(object_type, IApplicationEventPublisherAware)
        self.__is_application_context_aware = issubclass(object_type, IApplicationContextAware)
        self.__is_initializing_object = issubclass(object_type, IInitializingObject)
        self.__is_disposable_object = issubclass(object_type, IDisposableObject)
        self.__annotated_initializer = None
        self.__annotated_destroyer = None
        self.__dependencies = set(depends_on or [])
        self.__ctor_dependencies = None
        self.__autowired_properties = {}
        self.__autowired_methods = {}

        self.__find_annotated_methods()
        self.__find_autowired_members()

    def __find_annotated_methods(self):
        for method in self.__class_info.get_methods():
            is_initializing = method.has_annotation(OnInit)
            is_destroying = method.has_annotation(OnDestroy)

            if is_initializing and is_destroying:
                raise TypeError("Method can not be initializing and destroying simultaneously")
            elif is_initializing:
                if self.__annotated_initializer is None:
                    self.__annotated_initializer = method
                else:
                    raise TypeError("Class must have no more than 1 annotated initializing method")
            elif is_destroying:
                if self.__annotated_destroyer is None:
                    self.__annotated_destroyer = method
                else:
                    raise TypeError("Class must have no more than 1 annotated destroying method")

    def __get_method_dependencies(self, a_autowire):
        dependencies = {}
        if a_autowire:
            for arg_name in a_autowire.__attributes__:
                value = getattr(a_autowire, arg_name)
                if isinstance(value, six.string_types):
                    object_name = value
                    required_type = None
                elif isinstance(value, type):
                    object_name = None
                    required_type = value
                elif isinstance(value, dict) and len(value) == 1:
                    object_name, required_type = value.items()[0]
                else:
                    raise TypeError("Invalid Autowire annotation attributes")

                self.__dependencies.add((object_name, required_type))
                dependencies[arg_name] = (object_name, required_type)
        return dependencies

    def __find_autowired_members(self):
        a_autowire = self.__class_info.constructor.get_annotation(Autowire)
        self.__ctor_dependencies = self.__get_method_dependencies(a_autowire)

        for property in self.__class_info.get_properties():
            a_autowire = property.get_annotation(Autowire)
            if a_autowire:
                object_name = getattr(a_autowire, "name", None)
                required_type = getattr(a_autowire, "type", None)

                if object_name is None and required_type is None:
                    raise TypeError("Autowired property must specify at least one of: name, type")

                self.__dependencies.add((object_name, required_type))
                self.__autowired_properties[property] = (object_name, required_type)

        for method in self.__class_info.get_methods():
            a_autowire = method.get_annotation(Autowire)
            if a_autowire:
                self.__autowired_methods[method] = self.__get_method_dependencies(a_autowire)

    def get_object_name(self):
        return self.__object_name

    def get_object_type(self):
        return self.__object_type

    def is_singleton(self):
        return self.__is_singleton

    def get_annotation(self, annotation_type):
        return self.__class_info.get_annotation(annotation_type)

    def is_object_name_aware(self):
        return self.__is_object_name_aware

    def is_object_factory_aware(self):
        return self.__is_object_factory_aware

    def is_application_event_publisher_aware(self):
        return self.__is_application_event_publisher_aware

    def is_application_context_aware(self):
        return self.__is_application_context_aware

    def is_initializing_object(self):
        return self.__is_initializing_object

    def get_annotated_initializer(self):
        return self.__annotated_initializer

    def is_disposable_object(self):
        return self.__is_disposable_object

    def get_annotated_destroyer(self):
        return self.__annotated_destroyer

    def get_dependencies(self):
        return self.__dependencies

    def set_autowired_properties(self, existing_object, object_factory):
        for property, dependency in self.__autowired_properties.items():
            property.set(existing_object, object_factory.get_object(*dependency))

    def __wrap_autowired_method(self, info, method, dependencies, object_factory):
        arg_names = info.args

        def wrapped(*args, **kwargs):
            specified_args = set(arg_names[:len(args)])
            for arg_name, dependency in dependencies.items():
                if arg_name not in specified_args and arg_name not in kwargs:
                    kwargs[arg_name] = object_factory.get_object(*dependency)
            return method(*args, **kwargs)

        return wrapped

    def wrap_constructor(self, ctor, object_factory):
        if self.__ctor_dependencies:
            return self.__wrap_autowired_method(
                self.__class_info.constructor, ctor, self.__ctor_dependencies, object_factory
            )
        else:
            return ctor

    def wrap_autowried_methods(self, existing_object, object_factory):
        for method, dependencies in self.__autowired_methods.items():
            original_method = method.get(existing_object)
            wrapped_method = self.__wrap_autowired_method(method, original_method, dependencies, object_factory)
            method.set(existing_object, wrapped_method)


class DefaultObjectFactory(AliasRegistry):
    def __init__(self):
        super(DefaultObjectFactory, self).__init__()
        self.__definitions = {}
        self.__factories = {}
        self.__singletons = {}
        self.__object_post_processors = []
        self.__application_event_publisher = None
        self.__application_context = None

    def get_type(self, object_name):
        canonical_name = self.get_canonical_name(object_name)
        return self.__definitions[canonical_name].get_object_type()

    def is_singleton(self, object_name):
        canonical_name = self.get_canonical_name(object_name)
        return self.__definitions[canonical_name].is_singleton()

    def is_prototype(self, object_name):
        canonical_name = self.get_canonical_name(object_name)
        return not self.__definitions[canonical_name].is_singleton()

    def is_type_match(self, object_name, type_to_match):
        canonical_name = self.get_canonical_name(object_name)
        return issubclass(self.__definitions[canonical_name].get_object_type(), type_to_match)

    def find_annotation_on_object(self, object_name, annotation_type):
        canonical_name = self.get_canonical_name(object_name)
        return self.__definitions[canonical_name].get_annotation(annotation_type)

    @property
    def application_event_publisher(self):
        return self.__application_event_publisher

    @application_event_publisher.setter
    def application_event_publisher(self, application_event_publisher):
        self.__application_event_publisher = application_event_publisher

    @property
    def application_context(self):
        return self.__application_context

    @application_context.setter
    def application_context(self, application_context):
        self.__application_context = application_context

    @property
    def object_post_processors(self):
        return self.__object_post_processors

    @object_post_processors.setter
    def object_post_processors(self, object_post_processors):
        self.__object_post_processors = object_post_processors

    def set_properties(self, existing_object, object_name):
        canonical_name = self.get_canonical_name(object_name)
        definition = self.__definitions[canonical_name]

        if definition.is_object_name_aware():
            existing_object.set_object_name(canonical_name)

        if definition.is_object_factory_aware():
            existing_object.set_object_factory(self)

        if definition.is_application_event_publisher_aware():
            existing_object.set_application_event_publisher(self.__application_event_publisher)

        if definition.is_application_context_aware():
            existing_object.set_application_context(self.__application_context)

        return existing_object

    def post_process_before_initialization(self, existing_object, object_name):
        assert self.get_canonical_name(object_name)

        for object_post_processor in self.__object_post_processors:
            object_post_processor.post_process_before_initialization(existing_object, object_name)

        return existing_object

    def post_process_after_initialization(self, existing_object, object_name):
        assert self.get_canonical_name(object_name)

        for object_post_processor in self.__object_post_processors:
            object_post_processor.post_process_after_initialization(existing_object, object_name)

        return existing_object

    def initialize(self, existing_object, object_name):
        canonical_name = self.get_canonical_name(object_name)
        definition = self.__definitions[canonical_name]
        initializer_method = definition.get_annotated_initializer()

        if definition.is_initializing_object():
            existing_object.after_properties_set()

        if initializer_method:
            initializer_method.invoke_method(existing_object)

        return existing_object

    def destroy(self, existing_object, object_name):
        canonical_name = self.get_canonical_name(object_name)
        definition = self.__definitions[canonical_name]
        destroyer_method = definition.get_annotated_destroyer()

        if definition.is_disposable_object():
            existing_object.destroy()

        if destroyer_method:
            destroyer_method.invoke_method(existing_object)

        return existing_object

    def autowire(self, existing_object, object_name):
        canonical_name = self.get_canonical_name(object_name)
        definition = self.__definitions[canonical_name]

        definition.set_autowired_properties(existing_object, self)
        definition.wrap_autowried_methods(existing_object, self)

    def get_object(self, object_name=None, required_type=None, *args, **kwargs):
        if object_name is not None:
            canonical_name = self.get_canonical_name(object_name)
            definition = self.__definitions[canonical_name]

            if required_type is not None and not issubclass(definition.get_object_type(), required_type):
                raise LookupError("No object qualified by name '%s' and type %s found" % (object_name, required_type))

            is_singleton = definition.is_singleton()
            not_existing = canonical_name not in self.__singletons

            if not is_singleton or (is_singleton and not_existing):
                factory = self.__factories[canonical_name]
                instance = factory(*args, **kwargs)

                self.set_properties(instance, object_name)
                self.autowire(instance, object_name)
                self.post_process_before_initialization(instance, object_name)
                self.initialize(instance, object_name)
                self.post_process_after_initialization(instance, object_name)

                if is_singleton:
                    self.__singletons[canonical_name] = instance
            else:
                instance = self.__singletons[canonical_name]

            return instance
        elif required_type is not None:
            names = self.get_object_names_for_type(required_type, include_non_singletons=True)
            names_count = len(names)

            if names_count == 0:
                raise LookupError("No object qualified by type %s found" % required_type)
            elif names_count > 1:
                raise LookupError("Objects qualified by type %s is ambiguous" % required_type)
            else:
                return self.get_object(names[0], required_type, *args, **kwargs)
        else:
            raise TypeError("At least one of arguments must be specified: object_name, required_type")

    def get_object_names_for_annotation(self, annotation_type, include_non_singletons=False):
        object_names = []

        for object_name, definition in self.__definitions.items():
            match = (
                (include_non_singletons or definition.is_singleton()) and
                definition.get_annotation(annotation_type)
            )
            if match:
                object_names.append(object_name)

        return object_names

    def get_object_names_for_type(self, required_type, include_non_singletons=False):
        object_names = []

        for object_name, definition in self.__definitions.items():
            match = (
                (include_non_singletons or definition.is_singleton()) and
                issubclass(definition.get_object_type(), required_type)
            )
            if match:
                object_names.append(object_name)

        return object_names

    def get_objects_of_type(self, required_type, include_non_singletons=False):
        objects = {}

        for object_name in self.get_object_names_for_type(required_type, include_non_singletons):
            objects[object_name] = self.get_object(object_name)

        return objects

    def get_objects_with_annotation(self, annotation_type, include_non_singletons=False):
        objects = {}

        for object_name in self.get_object_names_for_annotation(annotation_type, include_non_singletons):
            objects[object_name] = self.get_object(object_name)

        return objects

    def _add_object_definition(self, definition, factory):
        object_name = definition.get_object_name()
        self._register_object_name(object_name)
        self.__definitions[object_name] = definition
        self.__factories[object_name] = factory

    def register_singleton(self, object_name, instance):
        self._add_object_definition(
            ObjectDefinition(object_name, type(instance), is_singleton=True),
            lambda: instance
        )

    def register_object_type(self, object_name, object_type, is_singleton):
        definition = ObjectDefinition(object_name, object_type, is_singleton)
        self._add_object_definition(
            definition,
            definition.wrap_constructor(object_type, self)
        )

    def register_factory_object(self, object_name, factory_object):
        self._add_object_definition(
            ObjectDefinition(object_name, factory_object.get_object_type(), factory_object.is_singleton()),
            factory_object.get_object
        )

    def unregister_object(self, object_name):
        canonical_name = self.get_canonical_name(object_name)
        if canonical_name in self.__singletons:
            self.destroy(self.__singletons[canonical_name], object_name)
            del self.__singletons[canonical_name]
        del self.__factories[canonical_name]
        self._unregister_object_name(object_name)

    def close(self):
        for object_name in self.__definitions.copy():
            self.unregister_object(object_name)

    def get_dependency_topology(self, *names):
        color = dict(map(lambda x: (x, 0), self.__definitions.keys()))
        result = []

        def dfs(object_name):
            if color[object_name] == 1:
                raise ReferenceError("Cyclic dependency found", [object_name])
            elif color[object_name] == 2:
                return

            try:
                color[object_name] = 1

                for required_name, required_type in self.__definitions[object_name].get_dependencies():
                    if required_name is not None:
                        if required_name not in self.__definitions:
                            raise LookupError(
                                "Unsatisfied dependency qualified by name '%s' of object '%s'" % (
                                    required_name, object_name
                                )
                            )
                        definition = self.__definitions[required_name]
                        if required_type is not None and not issubclass(definition.get_object_type(),
                                                                        required_type):
                            raise LookupError(
                                "Unsatisfied dependency qualified by name '%s' and type %s of object '%s'" % (
                                    required_name, required_type, object_name
                                )
                            )
                        dfs(required_name)
                    elif required_type is not None:
                        names = self.get_object_names_for_type(required_type, include_non_singletons=True)
                        names_count = len(names)
                        if names_count == 0:
                            raise LookupError(
                                "Unsatisfied dependency qualified by type %s of object '%s'" % (
                                    required_type, object_name
                                )
                            )
                        elif names_count > 1:
                            raise LookupError(
                                "Ambiguous dependency qualified by type %s of object '%s'" % (
                                    required_type, object_name
                                )
                            )
                        dfs(names[0])

                color[object_name] = 2

                result.append(object_name)
            except ReferenceError as e:
                if len(e.args[1]) == 1 or e.args[1][0] != e.args[1][-1]:
                    e.args[1].insert(0, object_name)
                raise e

        for object_name in names or self.__definitions:
            if object_name not in color:
                raise LookupError("No object qualified by name '%s' found" % object_name)

            if color[object_name] == 0:
                dfs(object_name)

        return result


class DefaultApplicationContext(IApplicationContext):
    def __init__(self):
        self.__object_factory = self._create_object_factory()
        self.__object_factory.application_context = weakref.proxy(self)
        self.__object_factory.application_event_publisher = weakref.proxy(self)
        self.__is_running = False

    def _create_object_factory(self):
        return DefaultObjectFactory()

    def get_object_factory(self):
        return self.__object_factory

    def contains_object(self, object_name):
        return self.__object_factory.contains_object(object_name)

    def get_aliases(self, object_name):
        return self.__object_factory.get_aliases(object_name)

    def get_object(self, object_name=None, required_type=None, *args, **kwargs):
        return self.__object_factory.get_object(object_name, required_type, *args, **kwargs)

    def get_type(self, object_name):
        return self.__object_factory.get_type(object_name)

    def is_prototype(self, object_name):
        return self.__object_factory.is_prototype(object_name)

    def is_singleton(self, object_name):
        return self.__object_factory.is_singleton(object_name)

    def is_type_match(self, object_name, type_to_match):
        return self.__object_factory.is_type_match(object_name, type_to_match)

    def find_annotation_on_object(self, object_name, annotation_type):
        return self.__object_factory.find_annotation_on_object(object_name, annotation_type)

    def get_objects_count(self):
        return self.__object_factory.get_objects_count()

    def get_objects_names(self):
        return self.__object_factory.get_objects_names()

    def get_object_names_for_annotation(self, annotation_type, include_non_singletons=False):
        return self.__object_factory.get_object_names_for_annotation(annotation_type, include_non_singletons)

    def get_object_names_for_type(self, required_type, include_non_singletons=False):
        return self.__object_factory.get_object_names_for_type(required_type, include_non_singletons)

    def get_objects_of_type(self, required_type, include_non_singletons=False):
        return self.__object_factory.get_objects_of_type(required_type, include_non_singletons)

    def get_objects_with_annotation(self, annotation_type, include_non_singletons=False):
        return self.__object_factory.get_objects_with_annotation(annotation_type, include_non_singletons)

    def register_singleton(self, object_name, instance):
        return self.__object_factory.register_singleton(object_name, instance)

    def register_object_type(self, object_name, object_type, is_singleton):
        return self.__object_factory.register_object_type(object_name, object_type, is_singleton)

    def register_factory_object(self, object_name, factory_object):
        return self.__object_factory.register_factory_object(object_name, factory_object)

    def unregister_object(self, object_name):
        return self.__object_factory.unregister_object(object_name)

    def publish_event(self, event):
        for listener in self.get_objects_of_type(IApplicationListener).values():
            listener.on_application_event(event)

    def close(self):
        if self.__is_running:
            self.stop()
        self.__object_factory.close()

    def is_running(self):
        return self.__is_running

    def start(self):
        names = self.__object_factory.get_object_names_for_type(ILifeCycle)
        topology = self.__object_factory.get_dependency_topology(*names)
        for object_name in topology:
            if self.is_singleton(object_name) and self.is_type_match(object_name, ILifeCycle):
                self.get_object(object_name).start()
        self.__is_running = True

    def stop(self):
        if not self.__is_running:
            return
        names = self.__object_factory.get_object_names_for_type(ILifeCycle)
        topology = self.__object_factory.get_dependency_topology(*names)
        topology.reverse()
        for object_name in topology:
            if self.is_singleton(object_name) and self.is_type_match(object_name, ILifeCycle):
                self.get_object(object_name).stop()
        self.__is_running = False

    def register_shutdown_hook(self):
        import atexit

        atexit.register(self.close)
