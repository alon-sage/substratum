# encoding: UTF-8

from substratum.core.annotation import Annotation, ElementType


class Configuration(Annotation):
    __target__ = [ElementType.CLASS]

    @staticmethod
    def _validate_attributes(name):
        pass


class ComponentScan(Annotation):
    __target__ = [ElementType.CLASS]

    @staticmethod
    def _validate_attributes(modules):
        pass


class ImportConfigClasses(Annotation):
    __target__ = [ElementType.CLASS]

    @staticmethod
    def _validate_attributes(classes):
        pass


class Produces(Annotation):
    __target__ = [ElementType.METHOD]

    @staticmethod
    def _validate_attributes(type):
        pass


class ObjectPostProcessor(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]

    @staticmethod
    def _validate_attributes(name):
        pass


class Component(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]

    @staticmethod
    def _validate_attributes(name):
        pass


class DependsOn(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]
    __repeatable__ = True

    @staticmethod
    def _validate_attributes(type, name):
        pass


class Prototype(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]


Stateful = Prototype


class Singleton(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]


Stateless = Singleton


class Alias(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]
    __repeatable__ = True

    @staticmethod
    def _validate_attributes(name):
        pass


class Primary(Annotation):
    __target__ = [ElementType.CLASS, ElementType.METHOD]
