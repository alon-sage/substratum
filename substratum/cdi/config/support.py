# encoding: UTF-8

import re
import functools

from substratum.cdi.config.annotations import Configuration, Component, \
    ObjectPostProcessor, Alias, Produces, ImportConfigClasses, ComponentScan, Singleton, Prototype, DependsOn
from substratum.cdi.config.interfaces import IComponentNamingStrategy, IConfigurableObjectFactory, \
    IConfigurableApplicationContext
from substratum.cdi.interfaces import IObjectPostProcessor
from substratum.cdi.interfaces import IObjectFactoryAware
from substratum.cdi.support import DefaultObjectFactory, ObjectDefinition, DefaultApplicationContext
from substratum.core.reflection.info import ClassInfo
from substratum.core.reflection.scanner import ModuleScanner


class ConfigurablePostProcessor(IObjectPostProcessor, IObjectFactoryAware):
    def __init__(self):
        self.__object_factory = None

    def set_object_factory(self, object_factory):
        self.__object_factory = object_factory

    def __make_config_wrapper(self, func, object_name):
        @functools.wraps(func)
        def wrapped():
            return self.__object_factory.get_object(object_name)

        return wrapped

    def post_process_before_initialization(self, obj, object_name):
        if self.__object_factory.find_annotation_on_object(object_name, Configuration):
            info = ClassInfo(self.__object_factory.get_type(object_name))

            for method in info.get_methods():
                if method.has_annotation(Component):
                    object_name = self.__object_factory.add_component(method, obj)
                    method.set(obj, self.__make_config_wrapper(method.get(obj), object_name))
                elif method.has_annotation(ObjectPostProcessor):
                    object_name = self.__object_factory.add_object_post_processor(method, obj)
                    method.set(obj, self.__make_config_wrapper(method.get(obj), object_name))

    def post_process_after_initialization(self, obj, object_name):
        pass


class DefaultComponentNamingStrategy(IComponentNamingStrategy):
    __FIRST_CAP_RE = re.compile(r'(.)([A-Z][a-z]+)')
    __ALL_CAP_RE = re.compile(r'([a-z0-9])([A-Z])')

    def convert(self, name):
        s1 = self.__FIRST_CAP_RE.sub(r'\1_\2', name)
        return self.__ALL_CAP_RE.sub(r'\1_\2', s1).lower()

    def get_component_name(self, info, component_annotation_type):
        a_component = info.get_annotation(component_annotation_type)
        return a_component.name or self.convert(info.name)


class ConfigurableObjectFactory(DefaultObjectFactory, IConfigurableObjectFactory):
    CONFIGURABLE_POST_PROCESSOR_NAME = "configurable_post_processor"

    def __init__(self):
        super(ConfigurableObjectFactory, self).__init__()
        self.__component_naming_strategy = DefaultComponentNamingStrategy()
        self.register_singleton(self.CONFIGURABLE_POST_PROCESSOR_NAME, ConfigurablePostProcessor())
        self.object_post_processors.append(self.get_object(self.CONFIGURABLE_POST_PROCESSOR_NAME))

    @property
    def component_naming_strategy(self):
        return self.__component_naming_strategy

    @component_naming_strategy.setter
    def component_naming_strategy(self, component_naming_strategy):
        self.__component_naming_strategy = component_naming_strategy

    def scan_for_components(self, modules):
        for name, type in ModuleScanner(modules):
            info = ClassInfo(type)

            if info.has_annotation(Configuration):
                self.add_configuration(info)
            elif info.has_annotation(Component):
                self.add_component(info)
            elif info.has_annotation(ObjectPostProcessor):
                self.add_object_post_processor(info)

    def add_config_class(self, config_class):
        info = ClassInfo(config_class)

        if not info.has_annotation(Configuration):
            raise TypeError("Configuration class must be annotation with Configuration")

        return self.add_configuration(info)

    def __get_component_name(self, info, component_annotation_type):
        return self.__component_naming_strategy.get_component_name(info, component_annotation_type)

    def __register_component_aliases(self, info, object_name):
        a_aliases = info.get_annotation(Alias) or []
        for a_alias in a_aliases:
            self.register_alias(object_name, a_alias.name)

    def __get_factory_method_type(self, info):
        a_produces = info.get_annotation(Produces)
        if not a_produces:
            raise TypeError("Configuration factory method must be annotated with Produces")
        return a_produces.type

    def add_configuration(self, info):
        object_name = self.__get_component_name(info, Configuration)

        a_import_config_classes = info.get_annotation(ImportConfigClasses)
        classes = getattr(a_import_config_classes, "classes", None) or []
        for cls in classes:
            self.add_config_class(cls)

        a_component_scan = info.get_annotation(ComponentScan)
        modules = getattr(a_component_scan, "modules", None) or []
        self.scan_for_components(modules)

        self.register_object_type(object_name, info.cls, is_singleton=True)

        self.__register_component_aliases(info, object_name)

        self.get_object(object_name)

        return object_name

    def add_component(self, info, config=None):
        object_name = self.__get_component_name(info, Component)

        a_singleton = info.get_annotation(Singleton)
        a_prototype = info.get_annotation(Prototype)

        if a_singleton and a_prototype:
            raise TypeError("Annotations Stateless and Statefull are mutually exclusive")
        elif a_singleton:
            is_singleton = True
        elif a_prototype:
            is_singleton = False
        else:
            is_singleton = True

        a_depends_on = info.get_annotation(DependsOn)
        dependencies = [(dep.name, dep.type) for dep in a_depends_on or []]

        if isinstance(info, ClassInfo):
            definition = ObjectDefinition(object_name, info.cls, is_singleton, dependencies)
            self._add_object_definition(
                definition,
                definition.wrap_constructor(info.cls, self)
            )
        else:
            object_type = self.__get_factory_method_type(info)
            self._add_object_definition(
                ObjectDefinition(object_name, object_type, is_singleton, dependencies),
                info.get(config)
            )

        self.__register_component_aliases(info, object_name)

        return object_name

    def add_object_post_processor(self, info, config=None):
        object_name = self.__get_component_name(info, ObjectPostProcessor)

        if isinstance(info, ClassInfo):
            self.register_object_type(object_name, info.cls, is_singleton=True)
        else:
            object_type = self.__get_factory_method_type(info)
            self._add_object_definition(
                ObjectDefinition(object_name, object_type, is_singleton=True),
                info.get(config)
            )

        self.__register_component_aliases(info, object_name)

        self.object_post_processors.append(self.get_object(object_name))

        return object_name


class ConfigurableApplicationContext(DefaultApplicationContext, IConfigurableApplicationContext):
    def _create_object_factory(self):
        return ConfigurableObjectFactory()

    def scan_for_components(self, modules):
        self.get_object_factory().scan_for_components(modules)

    def add_config_class(self, config_class):
        return self.get_object_factory().add_config_class(config_class)
