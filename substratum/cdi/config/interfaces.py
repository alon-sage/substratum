# encoding: UTF-8

from abc import *

from substratum.cdi.interfaces import IObjectFactory, IApplicationContext


class IComponentNamingStrategy(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_component_name(self, info, component_annotation_type):
        """
        Get component name based on component type.

        :type compopnent_type: type
        :type component_annotation_type: type
        :rtype: str
        """


class IConfigurableObjectFactory(IObjectFactory):
    __metaclass__ = ABCMeta

    @abstractmethod
    def scan_for_components(self, modules):
        """
        Scane specified modules for components as Configuration, Component, etc.

        :type modules: list of module
        """

    @abstractmethod
    def add_config_class(self, config_class):
        """
        Process and apply configuration class.

        :type config_class: type
        """


class IConfigurableApplicationContext(IApplicationContext, IConfigurableObjectFactory):
    __metaclass__ = ABCMeta
