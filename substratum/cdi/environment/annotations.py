# encoding: UTF-8

from substratum.core.annotation import Annotation, ElementType


class ValueRoot(Annotation):
    __target__ = [ElementType.CLASS]

    @staticmethod
    def _validate_attributes(root):
        pass


class Value(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(name, required):
        pass


class Default(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(value):
        pass
