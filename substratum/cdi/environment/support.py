# encoding: UTF-8

from abc import *
import json

from substratum.cdi.config.annotations import ObjectPostProcessor, Produces, Component
from substratum.cdi.environment.annotations import ValueRoot, Value, Default
from substratum.cdi.environment.interfaces import IEnvironment, IEnvironmentAware
from substratum.cdi.interfaces import IObjectPostProcessor, IObjectFactoryAware
from substratum.core.reflection.info import ClassInfo


class FileEnvironment(IEnvironment):
    def __init__(self, filename, mode="r", loader=json.load):
        with open(filename, mode) as f:
            self.__values = loader(f) or {}

    def get_value(self, name):
        path = name.split(".")
        try:
            current = self.__values
            while path:
                current = current[path.pop(0)]
            return current
        except KeyError:
            raise LookupError("No environment value '%s' found" % name)


class DictEnvironment(IEnvironment):
    def __init__(self, **values):
        self.__values = values

    @property
    def values(self):
        return self.__values

    @values.setter
    def values(self, value):
        self.__values = value

    def get_value(self, name):
        path = name.split(".")
        try:
            current = self.__values
            while path:
                current = current[path.pop(0)]
            return current
        except KeyError:
            raise LookupError("No environment value '%s' found" % name)


class EnvironmentContainer(IEnvironment):
    def __init__(self):
        self.__environments = []

    @property
    def environments(self):
        return self.__environments

    @environments.setter
    def environments(self, value):
        self.__environments = value

    def get_value(self, name):
        for environemnt in self.__environments:
            try:
                return environemnt.get_value(name)
            except LookupError:
                continue

        raise LookupError("No environment value '%s' found" % name)


class EnvironmentPostProcessor(IObjectPostProcessor, IObjectFactoryAware):
    def __init__(self, environment):
        self.__environment = environment
        self.__object_factory = None

    def set_object_factory(self, object_factory):
        self.__object_factory = object_factory

    def post_process_before_initialization(self, obj, object_name):
        type = self.__object_factory.get_type(object_name)
        info = ClassInfo(type)

        if issubclass(type, IEnvironmentAware):
            obj.set_environment(self.__environment)

        a_value_root = info.get_annotation(ValueRoot)
        root = None
        if a_value_root:
            root = a_value_root.root or object_name

        for property in info.get_properties():
            a_value = property.get_annotation(Value)
            if a_value:
                name = a_value.name or property.name
                if root:
                    name = root + "." + name
                required = a_value.required
                if required is None:
                    required = True

                a_default = property.get_annotation(Default)

                try:
                    value = self.__environment.get_value(name)
                    property.set(obj, value)
                except LookupError as e:
                    if a_default:
                        property.set(obj, getattr(a_default, "value", None))
                    elif required:
                        raise TypeError(str(e))

    def post_process_after_initialization(self, obj, object_name):
        pass


class EnvironmentConfigSupport(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def _setup_environment(self, container):
        pass

    @Component
    @Produces(type=EnvironmentContainer)
    def environment(self):
        container = EnvironmentContainer()
        self._setup_environment(container)
        return container

    @ObjectPostProcessor
    @Produces(type=EnvironmentPostProcessor)
    def environment_post_processor(self):
        return EnvironmentPostProcessor(self.environment())
