# encoding: UTF-8

from abc import *


class IEnvironment(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_value(self, name):
        """
        Get named value or raise LookupError.

        :type name: str
        :rtype: object
        """


class IEnvironmentAware(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_environment(self, environment):
        """
        Set envrionment.

        :type environment: IEnvironment
        """
