# encoding: UTF-8

from abc import *


class IObjectNameAware(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_object_name(self, object_name):
        """Set the object name."""


class IObjectFactoryAware(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_object_factory(self, object_factory):
        """Set object factory."""


class IApplicationEventPublisherAware(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_application_event_publisher(self, application_event_publisher):
        """Set application event publisher."""


class IApplicationContextAware(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def set_application_context(self, application_context):
        """Set application context."""


class IInitializingObject(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def after_properties_set(self):
        """
        Invoked by a IObjectFactory after it has set all object properties
        supplied.
        """


class IDisposableObject(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def destroy(self):
        """Invoked by a IObjectFactory on destruction of a singleton."""


class IFactoryObject(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_object(self):
        """Create object shared or independent."""

    @abstractmethod
    def get_object_type(self):
        """Get produced object type."""

    @abstractmethod
    def is_singleton(self):
        """Is factory prodeces singleton?"""


class IObjectPostProcessor(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def post_process_before_initialization(self, obj, object_name):
        """
        Apply this IObjectPostProcessor to the given new object instance
        before any object initialization callbacks.
        """

    @abstractmethod
    def post_process_after_initialization(self, obj, object_name):
        """
        Apply this IObjectPostProcessor to the given new object instance
        after any object initialization callbacks.
        """


class IObjectFactory(object):
    """
    Object factory implementations should support the standard object lifecycle
    interfaces as far as possible. The full set of initialization methods and
    their standard order is:
    1.  IObjectNameAware's set_object_name
    2.  IObjectFactoryAware's set_object_factory
    3.  IApplicationEventPublisherAware's set_application_event_publisher
    4.  IApplicationContextAware's set_application_context
    5.  post_process_before_initialization methods of IObjectPostProcessors
    6.  IInitializingObject's after_properties_set
    7.  a custom init-method definition
    8.  post_process_after_initialization methods of IObjectPostProcessors

    On shutdown of a object factory, the following lifecycle methods apply:
    1.  IDisposableObject's destroy
    2.  a custom destroy-method definition
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def contains_object(self, object_name):
        """
        Does object factory contains object definition or externally registered
        singleton instance with the given name?

        :type object_name: str
        :rtype: bool
        """

    @abstractmethod
    def get_aliases(self, object_name):
        """
        Return the aliases for the given object name, if any.

        :type object_name: str
        :rtype: list of str
        """

    @abstractmethod
    def get_object(self, object_name=None, required_type=None, *args, **kwargs):
        """
        Return an instance, which may be shared or independent, of the specified
        object.

        :type object_name: str
        :type required_type: type
        :rtype: object
        """

    @abstractmethod
    def get_type(self, object_name):
        """
        Determine the type of the object with the given name.

        :type object_name: str
        :rtype: type
        """

    @abstractmethod
    def is_prototype(self, object_name):
        """
        Is this object a prototype? That is, will
        `get_object(name, type, *args, **kwargs)`
        always return independent instances?

        :type object_name: str
        :rtype: bool
        """

    @abstractmethod
    def is_singleton(self, object_name):
        """
        Is this object a singleton? That is, will
        `get_object(name, type, *args, **kwargs)`
        always return the same instance?

        :type object_name: str
        :rtype: bool
        """

    @abstractmethod
    def is_type_match(self, object_name, type_to_match):
        """
        Check whether the object with the given name matches the specified type.
        :type object_name: str
        :type type_to_match: type
        :rtype: bool
        """

    @abstractmethod
    def find_annotation_on_object(self, object_name, annotation_type):
        """
        Find an Annotation of annotationType on the specified object, traversing
        its interfaces and super classes if no annotation can be found on the
        given class itself.

        :type object_name: str
        :type annotation_type: type
        :rtype: substratum.core.annotation.Annotation
        """

    @abstractmethod
    def get_objects_count(self):
        """
        Return the number of objects defined in the factory.

        :rtype: int
        """

    @abstractmethod
    def get_objects_names(self):
        """
        Return the names of all objects defined in this factory.

        :rtype: list of str
        """

    @abstractmethod
    def get_object_names_for_annotation(self, annotation_type, include_non_singletons=False):
        """
        Find all names of objects whose Class has the supplied Annotation type,
        without creating any object instances yet.

        :type annotation_type: type
        :rtype: list of str
        """

    @abstractmethod
    def get_object_names_for_type(self, required_type, include_non_singletons=False):
        """
        Return the names of objects matching the given type (including
        subclasses), judging from either object definitions or the value of
        object_type in the case of IFactoryObjects.

        :type required_type: type
        :type include_non_singletons: bool
        :type allow_eager_init: bool
        :rtype: list of str
        """

    @abstractmethod
    def get_objects_of_type(self, required_type, include_non_singletons=False):
        """
        Return the object instances that match the given object type (including
        subclasses), judging from either object definitions or the value of
        object_type in the case of IFactoryObjects.

        :type required_type: type
        :type include_non_singletons: bool
        :type allow_eager_init: bool
        :rtype: dict[str, object]
        """

    @abstractmethod
    def get_objects_with_annotation(self, annotation_type, include_non_singletons=False):
        """
        Find all objects whose Class has the supplied Annotation type, returning
        a dict of object names with corresponding object instances.

        :type annotation_type: type
        :rtype: dict[str, object]
        """

    @abstractmethod
    def register_singleton(self, object_name, instance):
        """
        Register singleton instance

        :type object_name: str
        :type instance: object
        """

    @abstractmethod
    def register_object_type(self, object_name, object_type, is_singleton):
        """
        Register object type.

        :type object_name: str
        :type object_type: type
        :type is_singleton: bool
        """

    @abstractmethod
    def register_factory_object(self, object_name, factory_object):
        """
        Register object factory.

        :type object_name: str
        :type factory_object: IFactoryObject
        """

    @abstractmethod
    def unregister_object(self, object_name):
        """
        Destroy and unregister object.

        :type object_name: str
        """

    @abstractmethod
    def close(self):
        """
        Destroy all objects and remove all definitions.
        """


class IApplicationEventPublisher(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def publish_event(self, event):
        """
        Send event to listeners.

        :type event: object
        """


class IApplicationListener(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def on_application_event(self, event):
        """
        Handle application event.

        :type event: object
        """


class ILifeCycle(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def is_running(self):
        """
        Check is component running.

        :rtype: bool
        """

    @abstractmethod
    def start(self):
        """
        Start component.
        """

    @abstractmethod
    def stop(self):
        """
        Stop component if not stopped yet.
        """


class IApplicationContext(IObjectFactory, IApplicationEventPublisher, ILifeCycle):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_object_factory(self):
        """
        Returns underlaying object factory.

        :rtype: IObjectFactory
        """
