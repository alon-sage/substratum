# encoding: UTF-8

from abc import *

import six

from substratum.core.annotation import AnnotationUtils
from substratum.core.reflection.info import ClassInfo
from substratum.serialization.annotations import *
from substratum.serialization.interfaces import ISerializer


class SerializerAdapter(ISerializer):
    __metaclass__ = ABCMeta

    def __init__(self):
        super(SerializerAdapter, self).__init__()
        self.__conversion_service = None
        self.__force_native = False

    @property
    def conversion_service(self):
        return self.__conversion_service

    @conversion_service.setter
    def conversion_service(self, value):
        self.__conversion_service = value

    @property
    def force_native(self):
        return self.__force_native

    @force_native.setter
    def force_native(self, value):
        self.__force_native = value

    def _is_natively_supported(self, type):
        return False

    def _get_property_field_name(self, member_info):
        a_name = member_info.get_annotation(FieldName)
        return getattr(a_name, "name", None) or member_info.name

    def _do_pre_encode_property_value(self, value, member_info):
        if value is None:
            return None

        a_field_type = member_info.get_annotation(FieldType)
        a_list_field = member_info.get_annotation(ListField)
        a_dict_field = member_info.get_annotation(DictField)
        a_field_item_type = member_info.get_annotation(FieldItemType)
        a_field_key_type = member_info.get_annotation(FieldKeyType)

        field_type = getattr(a_field_type, "target", None)
        field_key_type = getattr(a_field_key_type, "target", None)
        field_item_type = getattr(a_field_item_type, "target", None)

        if a_dict_field:
            return {
                self._do_pre_encode(k, field_key_type, member_info):
                    self._do_pre_encode(v, field_item_type, member_info)
                for k, v in value.items()
                }
        elif a_list_field:
            return [self._do_pre_encode(x, field_item_type, member_info) for x in value]
        else:
            return self._do_pre_encode(value, field_type, member_info)

    def _do_pre_encode_property(self, obj, member_info):
        return self._do_pre_encode_property_value(member_info.get(obj), member_info)

    def _do_pre_encode(self, obj, expected_type, member_info=None):
        if obj is None:
            return None
        elif isinstance(expected_type, six.class_types):
            if self.__force_native and self._is_natively_supported(type(obj)):
                return obj
            else:
                return self.__conversion_service.convert_if_necessary(obj, expected_type, member_info)
        else:
            cls_info = ClassInfo(type(obj))
            a_serializable = cls_info.get_annotation(SerializableType)

            if a_serializable:
                result = {}

                serialize_properties = a_serializable.serialize_properties
                if serialize_properties is None:
                    serialize_properties = True

                for property in cls_info.get_properties():
                    serializable = serialize_properties or property.has_annotation(SerializableField)
                    transient = property.has_annotation(TransientField)
                    read_only = property.has_annotation(ReadOnlyFiled)
                    if serializable and not transient and not read_only:
                        field_name = self._get_property_field_name(property)
                        result[field_name] = self._do_pre_encode_property(obj, property)

                return result
            else:
                return obj

    def _do_post_decode_property_value(self, decoded, member_info):
        if decoded is None:
            return None

        a_type = member_info.get_annotation(FieldType)
        a_list_field = member_info.get_annotation(ListField)
        a_dict_field = member_info.get_annotation(DictField)
        a_item_type = member_info.get_annotation(FieldItemType)
        a_key_type = member_info.get_annotation(FieldKeyType)

        field_type = getattr(a_type, "source", None)
        field_key_type = getattr(a_key_type, "source", None)
        field_item_type = getattr(a_item_type, "source", None)

        if a_dict_field:
            post_decoded = {
                self._do_post_decode(k, field_key_type, member_info):
                    self._do_post_decode(v, field_item_type, member_info)
                for k, v in decoded.items()
                }
        elif a_list_field:
            post_decoded = [self._do_post_decode(x, field_item_type, member_info) for x in decoded]
        else:
            post_decoded = self._do_post_decode(decoded, field_type, member_info)

        return post_decoded

    def _do_post_decode_property(self, decoded, obj, member_info):
        member_info.set(obj, self._do_post_decode_property_value(decoded, member_info))

    def _do_post_decode(self, decoded, expected_type, member_info=None):
        if decoded is None:
            return None
        elif isinstance(expected_type, six.class_types):
            a_serializable = AnnotationUtils.get_annotation(expected_type, SerializableType)

            if a_serializable:
                cls_info = ClassInfo(expected_type)
                obj = expected_type()

                serialize_properties = a_serializable.serialize_properties
                if serialize_properties is None:
                    serialize_properties = True

                for property in cls_info.get_properties():
                    serializable = serialize_properties or property.has_annotation(SerializableField)
                    transient = property.has_annotation(TransientField)
                    write_only = property.has_annotation(WriteOnlyFiled)
                    if serializable and not transient and not write_only:
                        field_name = self._get_property_field_name(property)
                        self._do_post_decode_property(decoded.get(field_name, None), obj, property)

                return obj
            else:
                if self.__force_native and self._is_natively_supported(type(decoded)):
                    return decoded
                else:
                    return self.__conversion_service.convert_if_necessary(decoded, expected_type, member_info)
        else:
            return decoded

    @abstractmethod
    def _do_encode(self, pre_encoded):
        pass

    @abstractmethod
    def _do_decode(self, encoded):
        pass

    def encode_property_value(self, value, member_info):
        pre_encoded = self._do_pre_encode_property_value(value, member_info)
        return self._do_encode(pre_encoded)

    def encode_property(self, obj, member_info):
        pre_encoded = self._do_pre_encode_property(obj, member_info)
        return self._do_encode(pre_encoded)

    def encode(self, obj, expected_type=None):
        pre_encoded = self._do_pre_encode(obj, expected_type)
        return self._do_encode(pre_encoded)

    def decode_property_value(self, encoded, member_info):
        decoded = self._do_decode(encoded)
        return self._do_post_decode_property_value(decoded, member_info)

    def decode_property(self, encoded, obj, member_info):
        decoded = self._do_decode(encoded)
        return self._do_post_decode_property(decoded, obj, member_info)

    def decode(self, encoded, expected_type=None):
        decoded = self._do_decode(encoded)
        return self._do_post_decode(decoded, expected_type)
