# encoding: UTF-8

from substratum.core.exception import CoreException


class SerializationException(CoreException):
    pass
