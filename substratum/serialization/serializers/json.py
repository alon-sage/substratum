# encoding: UTF-8

from __future__ import absolute_import

from json import JSONEncoder as _JSONEncoder, JSONDecoder as _JSONDecoder

from substratum.serialization.support import SerializerAdapter


class JSONSerializer(SerializerAdapter):
    def __init__(self):
        super(JSONSerializer, self).__init__()
        self.__encoder = _JSONEncoder()
        self.__decoder = _JSONDecoder()

    @property
    def encoder(self):
        return self.__encoder

    @encoder.setter
    def encoder(self, value):
        self.__encoder = value

    @property
    def decoder(self):
        return self.__decoder

    @decoder.setter
    def decoder(self, value):
        self.__decoder = value

    def _do_encode(self, pre_encoded):
        return self.__encoder.encode(pre_encoded)

    def _do_decode(self, encoded):
        return self.__decoder.decode(encoded)
