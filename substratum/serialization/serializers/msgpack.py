# encoding: UTF-8

from __future__ import absolute_import

from msgpack import Packer, Unpacker

from substratum.serialization.support import SerializerAdapter


class MsgpackSerializer(SerializerAdapter):
    def __init__(self):
        super(MsgpackSerializer, self).__init__()
        self.__encoder = Packer()
        self.__decoder = Unpacker()

    @property
    def encoder(self):
        return self.__encoder

    @encoder.setter
    def encoder(self, value):
        self.__encoder = value

    @property
    def decoder(self):
        return self.__decoder

    @decoder.setter
    def decoder(self, value):
        self.__decoder = value

    def _do_encode(self, pre_encoded):
        return self.__encoder.pack(pre_encoded)

    def _do_decode(self, encoded):
        self.__decoder.feed(encoded)
        return self.__decoder.unpack()
