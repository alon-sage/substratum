# encoding: UTF-8

from substratum.core.annotation import Annotation, ElementType


class SerializableType(Annotation):
    __target__ = [ElementType.CLASS]

    @staticmethod
    def _validate_attributes(serialize_properties):
        pass


class SerializableField(Annotation):
    __target__ = [ElementType.PROPERTY]


class TransientField(Annotation):
    __target__ = [ElementType.PROPERTY]


class FieldName(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(name):
        pass


class FieldType(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(source, target):
        pass


class ListField(Annotation):
    __target__ = [ElementType.PROPERTY]


class DictField(Annotation):
    __target__ = [ElementType.PROPERTY]


class FieldItemType(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(source, target):
        pass


class FieldKeyType(Annotation):
    __target__ = [ElementType.PROPERTY]

    @staticmethod
    def _validate_attributes(source, target):
        pass


class ReadOnlyFiled(Annotation):
    __target__ = [ElementType.PROPERTY]


class WriteOnlyFiled(Annotation):
    __target__ = [ElementType.PROPERTY]
