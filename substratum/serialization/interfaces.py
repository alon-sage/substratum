# encoding: UTF-8

from abc import *


class ISerializer(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def encode_property_value(self, value, member_info):
        pass

    @abstractmethod
    def encode_property(self, obj, member_info):
        pass

    @abstractmethod
    def encode(self, obj, expected_type=None):
        pass

    @abstractmethod
    def decode_property_value(self, encoded, member_info):
        pass

    @abstractmethod
    def decode_property(self, encoded, obj, member_info):
        pass

    @abstractmethod
    def decode(self, encoded, expected_type=None):
        pass
