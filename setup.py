from distutils.core import setup

from setuptools import find_packages

setup(
    name='substratum',
    version='1.0.0',
    packages=find_packages(exclude=["test"]),
    url='',
    license='',
    author='Ivan Babintsev',
    author_email='alon.sage@gmail.com',
    description='',
    install_requires=["six", "pypeg2"]
)
